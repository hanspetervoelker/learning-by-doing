module.exports = {
  // set your styleguidist configuration here
  title: "HPV's Music-Vue-Components",
  defaultExample: true,
  components: 'src/components/[A-Za-z]*.vue',
  usageMode: 'expand',
  exampleMode: 'expand',
  ignore: [
    'src/components/CoFKey.vue',
    'src/components/CoFCircle.vue',
    'src/components/CoFRing.vue',
  ],
  // sections: [
  //   {
  //     name: 'First Section',
  //     components: 'src/components/**/[A-Z]*.vue'
  //   }
  // ],
  // webpackConfig: {
  //   // custom config goes here
  // }
  sections: [
    {
      name: 'Introduction',
      content: 'docs/Introduction.md',
    },
    {
      name: 'Chords',
      components: 'src/components/*Chord*.vue',
      description: 'Components for displaying chords',
    },
    {
      name: 'Circle Of Fifths',
      components: 'src/components/*CoF*.vue',
      description: 'Circle Of Fifths Component and Subcomponents',
    },
    {
      name: 'Scales',
      components: 'src/components/+([sS]cale*.vue|KeySigns.vue|KeyPiano.vue)',
      description: 'Components for displaying scales',
    },
  ],
  editorConfig: {
    theme: 'zenburn',
  },
};
