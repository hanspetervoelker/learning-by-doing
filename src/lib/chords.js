/**
 * @file lib/chords.js
 * @example
 * import listOfChords, { majorScaleChords } from 'lib/chords.js';
 *
 * const dim = listOfChords.HalfDiminishedSeventh;
 * const ms = majorScaleChords[1];
*/

/**
 * chords object with properties to buid a chord
 * @typedef {Object} chordsObject
 * @property {string[]} intervals a list of string indicating the intervals (e.g. ['1', '♭3', '♭5', '♭♭7'])
 * @property {number[]} formula a formula where to find the required note in a 12-noteslist
 * @property {string} name name of the chord (without the key e.g. dim)
 * @property {string} extension extension the chord (e.g. 7b5)
 */

/**
 * list of chord-objects
 * @alias default
 * @type {Object}
 * @property {chordsObject} DiminishedSeventh e.g. Adim7
 * @property {chordsObject} HalfDiminishedSeventh e.g. Am7b5
 * @property {chordsObject} MinorSeventh e.g. Am7
 * @property {chordsObject} MinorMajorSeventh e.g. Ammaj7
 * @property {chordsObject} DominatSeventh e.g. A7
 * @property {chordsObject} MajoSeventh e.g. Amaj7
 * @property {chordsObject} AugmentedSeventh e.g. A7#5
 * @property {chordsObject} AugmentedMajorSeventh e.g. Amaj7#5
 */
const chords = {
  DiminishedSeventh: {
    intervals: ['1', '♭3', '♭5', '♭♭7'],
    formula: [0, 3, 6, 9],
    name: 'dim',
    extension: '7',
  },
  HalfDiminishedSeventh: {
    intervals: ['1', '♭3', '♭5', '♭7'],
    formula: [0, 3, 6, 10],
    name: 'm',
    extension: '7b5',
  },
  MinorSeventh: {
    intervals: ['1', '♭3', '5', '♭7'],
    formula: [0, 3, 7, 10],
    name: 'm',
    extension: '7',
  },
  MinorMajorSeventh: {
    intervals: ['1', '♭3', '5', '7'],
    formula: [0, 3, 7, 11],
    name: 'm',
    extension: 'maj7',
  },
  DominantSeventh: {
    intervals: ['1', '3', '5', '♭7'],
    formula: [0, 4, 7, 10],
    name: '',
    extension: '7',
  },
  MajorSeventh: {
    intervals: ['1', '3', '5', '7'],
    formula: [0, 4, 7, 11],
    name: '',
    extension: 'maj7',
  },
  AugmentedSeventh: {
    intervals: ['1', '3', '♯5', '♭7'],
    formula: [0, 4, 8, 10],
    name: 'aug',
    extension: '7',
  },
  AugmentedMajorSeventh: {
    intervals: ['1', '3', '♯5', '7'],
    formula: [0, 4, 8, 11],
    name: 'aug',
    extension: 'maj7',
  },
};

/**
 * List of (7) Chordtypes in a major scale (MajorSeventh, MinorSeventh, ...) in order of appearance in scale
 * @type string[]
 */
const majorScaleChords = [
  'MajorSeventh',
  'MinorSeventh',
  'MinorSeventh',
  'MajorSeventh',
  'DominantSeventh',
  'MinorSeventh',
  'HalfDiminishedSeventh',
];

export { chords as default, majorScaleChords };
