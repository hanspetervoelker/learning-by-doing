const flats = [
  'C',
  'Db',
  'D',
  'Eb',
  'E',
  'F',
  'Gb',
  'G',
  'Ab',
  'A',
  'Bb',
  'B',
];
const sharps = [
  'C',
  'C#',
  'D',
  'D#',
  'E',
  'F',
  'F#',
  'G',
  'G#',
  'A',
  'A#',
  'B',
];
const fsharps = [
  'C',
  'C#',
  'D',
  'D#',
  'E',
  'E#',
  'F#',
  'G',
  'G#',
  'A',
  'A#',
  'B',
];
const eflats = [
  'C',
  'Db',
  'D',
  'Eb',
  'E',
  'F',
  'Gb',
  'G',
  'Ab',
  'A',
  'Bb',
  'Cb',
];

/**
 * Object with Arrays of 12 notes for flat an sharp keys
 * @file lib/notes.js
 * @type {Object}
 * @property {string[]} flats notes with black keys set to flats
 * @property {string[]} sharps notes with black keys set to sharps
 * @property {string[]} fsharps notes with black keys set to sharps and f to e#
 */
export default {
  flats,
  sharps,
  fsharps,
  eflats,
};
