import Key from './keys';

/**
 * @file lib/circleOfFifths.js
 */

/**
 * Class presenting a Circle of Fifths
 * @class
 * @property {Key[]} keys the 24 keys of the cof
 */
export class CoF {
  /**
   * Creates a new circle of fiths
   * @constructor
   * @param {Key[]} keys 24 keys
   */
  constructor(keys) {
    this.keys = keys;
    /**
     * returns a key by its name
     * @method
     * @param {string} key major or minor
     * @param {string} name name of the key e.g. C, G, D
     * @returns {Key}
     */
    this.getByKeyAndName = (key, name) => this.keys.filter(
      k => k.key === key && k.name === name,
    )[0];
    /**
     * returns a key by its position
     * @method
     * @param {string} key major or minor
     * @param {number} pos name of the key e.g. 0, 1, 2
     * @returns {Key}
     */
    this.getByKeyAndPosition = (key, pos) => this.keys.filter(
      k => k.key === key && k.circleNbr === pos,
    )[0];
  }
}

/**
 * CoF instance with 24 keys
 * @alias default
 * @type {CoF}
 */
export default new CoF([
  new Key('major', 0, '♮', 'sharps', 'C', 0),
  new Key('major', 1, '♯', 'sharps', 'G', 1),
  new Key('major', 2, '♯', 'sharps', 'D', 2),
  new Key('major', 3, '♯', 'sharps', 'A', 3),
  new Key('major', 4, '♯', 'sharps', 'E', 4),
  new Key('major', 5, '♯', 'sharps', 'B', 5),
  new Key('major', 6, '♯', 'fsharps', 'F#', 6),
  new Key('major', 5, '♭', 'flats', 'Db', 7),
  new Key('major', 4, '♭', 'flats', 'Ab', 8),
  new Key('major', 3, '♭', 'flats', 'Eb', 9),
  new Key('major', 2, '♭', 'flats', 'Bb', 10),
  new Key('major', 1, '♭', 'flats', 'F', 11),
  new Key('minor', 0, '♯', 'sharps', 'A', 0),
  new Key('minor', 1, '♯', 'sharps', 'E', 1),
  new Key('minor', 2, '♯', 'sharps', 'B', 2),
  new Key('minor', 3, '♯', 'sharps', 'F♯', 3),
  new Key('minor', 4, '♯', 'sharps', 'C♯', 4),
  new Key('minor', 5, '♯', 'sharps', 'G♯', 5),
  new Key('minor', 6, '♯', 'fsharps', 'D♯', 6),
  new Key('minor', 5, '♯', 'flats', 'Bb', 7),
  new Key('minor', 4, '♭', 'flats', 'F', 8),
  new Key('minor', 3, '♭', 'flats', 'C', 9),
  new Key('minor', 2, '♭', 'flats', 'G', 10),
  new Key('minor', 1, '♭', 'flats', 'D', 11),
]);
