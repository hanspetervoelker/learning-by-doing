/**
 * Configuration for cof-component
 * @type {Object}
 * @property {Object} majorKeyStyle styling of a single key in the major ring (circle segment)
 * @property {Object} minorKeyStyle styling of a single key in the minor ring (circle segment)
 * @property {Object} majorRingStyle styling of the outer ring
 * @property {Object} minorRingStyle styling of the inner ring
 * @property {Object} circleStyle styling of the circle in the middle
 * @property {Object} textStyle styling of circle text
 * @property {boolean} showShadow shows shadow if true
 * @property {string} majorRingname name of outer ring
 * @property {string} minorRingname name of inner ring
 */
export default {
  majorKeyStyle: {
    fontSize: '1em',
    background: '#f9f9f9',
    paddingTop: '12px',
    border: '1px solid #999',
    color: '#343434',
    activeColor: '#f1f1f1',
    activeBackground: '#555',
  },
  minorKeyStyle: {
    fontSize: '0.8em',
    background: '#f1f1f1',
    paddingTop: '12px',
    border: '1px solid #999',
    color: '#343434',
    activeColor: '#f1f1f1',
    activeBackground: '#555',
  },
  majorRingStyle: {
    border: '1px solid #999',
    padding: '0',
    width: '20em',
    height: '20em',
    top: '0',
    left: '0',
  },
  minorRingStyle: {
    border: '1px solid #999',
    padding: '0',
    width: '15em',
    height: '15em',
    top: '2.5em',
    left: '2.5em',
  },
  circleStyle: {
    width: '10em',
    height: '10em',
    background: '#fff',
    color: '#343434',
    top: '5em',
    left: '5em',
    border: '1px solid #555',
  },
  textStyle: {
    'font-size': '2.5em',
    'font-weight': 'bold',
    color: '#343434',
  },
  showShadow: true,
  majorRingname: 'major',
  minorRingname: 'minor',
};
