/**
 * @typedef {Object} churchMode
 * @property {string} name name of the scale
 * @property {string[]} scaleintervals scale intervals (based on 7 tone scale)
 * @property {number[]} formula formula to creating the scale
 * @property {number} offset the offset in cof
 * @property {number} scalepos the position in a major scale(12) e.g. dorian = 2
 */


/**
 * Class presenting a scale (used for modal scales)
 * @property {string} name name of the scale
 * @property {string[]} formula the scale formula
 */
export class Scale {
  /**
   * @constructor
   * @param {string} name name of the scale e.g. Ionian
   * @param {number[]} formula formula (based on 12 notes) representing the scale
   */
  constructor(name, formula) {
    this.name = name || 'ionian';
    this.formula = formula || [0, 2, 4, 5, 7, 9, 11];
    /**
     * maps the notes of a key to the scale
     * @param {Key} key the key with notes
     * @returns {string[]}
     */
    this.getScaleNotes = key => this.formula.map(x => key.notes[x]);
  }
}

/**
 * List of scale formulas
 * @alias default
 * @type {Object}
 * @property {number[]} ionian formula of the ionian scale
 * @property {number[]} dorian formula of the dorian scale
 * @property {number[]} phygrian formula of the phygrian scale
 * @property {number[]} lydian formula of the lydian scale
 * @property {number[]} mixolydian formula of the mixolydian scale
 * @property {number[]} aeolian formula of the aelian scale
 * @property {number[]} locrian formula of the locrian scale
 */
export default {
  ionian: [0, 2, 4, 5, 7, 9, 11],
  dorian: [0, 2, 3, 5, 7, 9, 10],
  phrygian: [0, 1, 3, 5, 7, 8, 10],
  lydian: [0, 2, 4, 6, 7, 9, 11],
  mixolydian: [0, 2, 4, 5, 7, 9, 10],
  aeolian: [0, 2, 3, 5, 7, 8, 10],
  locrian: [0, 1, 3, 5, 6, 8, 10],
};

/**
 * Array of churchmode scales with formulas, intervals and offsets
 * @type {churchMode}
 */
export const churchModes = [
  {
    name: 'ionian',
    formula: [0, 2, 4, 5, 7, 9, 11],
    scaleintervals: ['1', '2', '3', '4', '5', '6', '7'],
    offset: 0,
    scalepos: 0,
  },
  {
    name: 'dorian',
    formula: [0, 2, 3, 5, 7, 9, 10],
    scaleintervals: ['1', '2', 'b3', '4', '5', '6', 'b7'],
    offset: 10,
    scalepos: 2,
  },
  {
    name: 'phygrian',
    formula: [0, 1, 3, 5, 7, 8, 10],
    scaleintervals: ['1', 'b2', 'b3', '4', '5', 'b6', 'b7'],
    offset: 8,
    scalepos: 4,
  },
  {
    name: 'lydian',
    formula: [0, 2, 4, 6, 7, 9, 11],
    scaleintervals: ['1', '2', '3', '#4', '5', '6', '7'],
    offset: 1,
    scalepos: 5,
  },
  {
    name: 'mixolydian',
    formula: [0, 2, 4, 5, 7, 9, 10],
    scaleintervals: ['1', '2', '3', '4', '5', '6', 'b7'],
    offset: 11,
    scalepos: 7,
  },
  {
    name: 'aeolian',
    formula: [0, 2, 3, 5, 7, 8, 10],
    scaleintervals: ['1', '2', 'b3', '4', '5', 'b6', 'b7'],
    offset: 9,
    scalepos: 9,
  },
  {
    name: 'locrian',
    formula: [0, 1, 3, 5, 6, 8, 10],
    scaleintervals: ['1', 'b2', 'b3', '4', 'b5', 'b6', 'b7'],
    offset: 7,
    scalepos: 11,
  },
];
