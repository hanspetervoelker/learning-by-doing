import notes from './notes';

/**
 * @file lib/keys.js
 * @module keys
 */

/**
 * Class presenting a key
 * @alias default
 * @property {number} offset the offset of the key to c
 * @property {string[]} notes the 12 notes starting with the root note of the key
 * @property {string} key one of [major, minor]
 * @property {number} nbrOfSigns the number of signs 0-6
 * @property {string} signType the type of the sign sharp, flat, natural e.g. #, b, ♮
 * @property {string} noteType the type of 12-notes-list to use e.g. sharps, flats
 * @property {string} name the name of the key e.g. A, F#
 * @property {number} circleNbr the position of the key in the circle of fithts
 */
export default class Key {
  /**
   * Creates a new Key
   * @constructor
   * @param {string} key one of [major, minor]
   * @param {number} nbrOfSigns the number of signs 0-6
   * @param {string} signType the type of the sign sharp, flat, natural e.g. #, b, ♮
   * @param {string} noteType the type of 12-notes-list to use e.g. sharps, flats
   * @param {string} name the name of the key e.g. A, F#
   * @param {number} circleNbr the position of the key in the circle of fithts
   */
  constructor(key, nbrOfSigns, signType, noteType, name, circleNbr) {
    this.key = key || 'major';
    this.nbrOfSigns = nbrOfSigns || 0;
    this.signType = signType || '♮';
    this.noteType = (notes[noteType] !== undefined) ? noteType : 'sharps';
    this.name = name || 'C';
    this.circleNbr = circleNbr || 0;
    this.unorderdNotes = notes[this.noteType];

    const setNotes = () => {
      let clone = notes[this.noteType].slice(0);
      const i = clone.indexOf(this.name);
      this.offset = i || 0;
      if (i && i > 0) {
        const right = clone.slice(i);
        clone = clone.splice(0, i);
        right.push(...clone);
        return right;
      }
      return clone;
    };

    this.notes = setNotes();
    /**
     * returns a list of notes of the key based on a scale formula
     * @method
     * @param {number[]} scaleFormula the scale formula the notes should be applied
     * @returns {string[]}
     */
    this.getScaleByFormula = scaleFormula => scaleFormula.map(item => this.notes[item]);
  }
}
