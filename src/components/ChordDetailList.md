```js
const majorScaleChords = require('../lib/chords').majorScaleChords;
const cofdata = require('../lib/circleOfFifths').default;
const ionian = [0, 2, 4, 5, 7, 9, 11];
const akey = cofdata.keys[2];
const showShadow = true;
const handleClick = function(obj) {
  alert(`You selected ${obj.fullname}`);
};

<chord-detail-list
  :chord-type-list="majorScaleChords"
  :akey="akey"
  :scale-formula="ionian"
  :showShadow="showShadow"
  v-on:chordlist-click="handleClick"/>
```