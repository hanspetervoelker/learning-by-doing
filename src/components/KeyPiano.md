```js
const cofdata = require('../lib/circleOfFifths').default;
const ionian = [0, 2, 4, 5, 7, 9, 11];

const data = {
  selected: 9,
};

<key-piano :akey="cofdata.keys[data.selected]" :formula="ionian">
  <select v-model="data.selected" style="margin-bottom: 8px">
    <option value="0">C</option>
    <option value="1">G</option>
    <option value="2">D</option>
    <option value="3">A</option>
    <option value="4">E</option>
    <option value="5">B</option>
    <option value="6">F#</option>
    <option value="7">Db</option>
    <option value="8">Ab</option>
    <option value="9">Eb</option>
    <option value="10">Bb</option>
    <option value="11">F</option>
  </select>
</key-piano>
```