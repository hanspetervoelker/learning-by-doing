```js
const cof = require('../lib/circleOfFifths').default;
new Vue({
  data(){
    return {
      scale: {
          name: 'ionian',
          formula: [0, 2, 4, 5, 7, 9, 11],
          scaleintervals: ['1', '2', '3', '4', '5', '6', '7'],
          offset: 0,
          scalepos: 0,
        },
        akey: cof.keys[3],
        showTop: true,
        standalone: true,
    };
  },
  template: `
    <div>
      <p>
        <input type="checkbox" v-model="showTop"/>showTop
        <input type="checkbox" v-model="standalone"/>standalone
      </p>
      <scale-info
        :ascale="scale"
        :akey="akey"
        :show-top="showTop"
        :standalone="standalone"/>
    </div>
  `
});
```