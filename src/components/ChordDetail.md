```js
const data = {
  chordType: 'HalfDiminishedSeventh',
  akey: {
    name: 'D',
    notes: ['D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B', 'C', 'Db'],
  },
  handler: function(dn, fn, ct) {
    alert(fn);
  },
};
<chord-detail :akey="data.akey" :chordType="data.chordType" v-on:chord-click="data.handler"/>
```