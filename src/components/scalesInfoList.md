```js
const churchModes = require('../lib/scales').churchModes;
const cofdata= require('../lib/circleOfFifths').default;
const akey = cofdata.keys[1];
let keyOffset = true;

<scales-info-list
  :akey="akey"
  :scales="churchModes"
  :use-key-offset="keyOffset">
  <input type="checkbox" v-model="keyOffset" style="margin-bottom: 10px"/>Use Offset
</scales-info-list>
```