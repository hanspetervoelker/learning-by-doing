```js
const cofConfig = require('../lib/cofConfig').default;
const cofdata = require('../lib/circleOfFifths').default;

const data = {
  config: cofConfig,
  majorKeys: cofdata.keys.filter(item => item.key === 'major'),
  minorKeys: cofdata.keys.filter(item => item.key === 'minor'),
  text: '♮',
};

const handler = function(obj) {
  const activeKey = data.majorKeys[obj.position];
  data.text = (activeKey.nbrOfSigns === 0)
    ? activeKey.signType
    : activeKey.nbrOfSigns + activeKey.signType;
};

<cof style="transform: scale(0.7); margin: -36px;"
 :config="data.config"
 :majorKeys="data.majorKeys"
 :minorKeys="data.minorKeys"
 :circleText="data.text"
 v-on:cof-position-change="handler"/>
```