```js
const handleEvent = function(chordName, fullName) {
  alert(`You clicked on ${fullName}`);
};

<pretty-chord-name chord-name="F#m"
                   chord-extension="7b5"
                   v-on:chord-name-click="handleEvent"
                   title="click to alert"
                   style="cursor: pointer">
```