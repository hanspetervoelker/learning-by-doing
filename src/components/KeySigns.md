```js
  const keyInfos = {
    flats: {
      nbrOfSigns: 3,
      signType: 'b',
    },
    sharps: {
      nbrOfSigns: 2,
      signType: '#',
    },
    natural: {
      nbrOfSigns: 0,
      signType: '#',
    }
  };
  <key-signs :akey="keyInfos.flats"/>
```