This is the documentation of some Vue-Single-File-Components (SFC) I made during learning Vue.

The components are not as customizable as I would wish. Because of my weakness in css I spent to much
time fighting with the layout and did some hardcoding in styles.

The components are not ready for production, but may be helpfull for other people to implements such
components or to look for inspiration. To be usable a documentation is necessary - this is the documentation
of the component usage.

Feel free to fork the repository and make this components better. This project is under the license of MIT, so
you can do with the code whatever you want.