import { shallowMount } from '@vue/test-utils';
import ring from '@/components/CoFRing.vue';
import cof from '../../src/lib/circleOfFifths';
import CoFKey from '@/components/CoFKey.vue';

describe('cof-ring test', () => {
  test('method call', () => {
    const spy = jest.fn();
    const comp = shallowMount(ring, {
      propsData: {
        cofKeys: cof.keys,
      },
    });
    expect(comp.exists()).toBe(true);
    comp.setMethods({ handleKeyClick: spy });
    comp.findAll(CoFKey).at(1).vm.$emit('cof-key-click', 1, 'G');

    expect(spy).toHaveBeenCalledWith(1, 'G');
  });
  test('data change', () => {
    const comp = shallowMount(ring, {
      propsData: {
        cofKeys: cof.keys,
      },
    });
    expect(comp.exists()).toBe(true);
    comp.findAll(CoFKey).at(1).vm.$emit('cof-key-click', 1, 'G');
    expect(comp.vm.$data.activePos).toBe(1);
    expect(comp.vm.$data.activeName).toBe('G');
  });
});
