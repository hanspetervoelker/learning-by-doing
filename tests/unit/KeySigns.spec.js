import { shallowMount } from '@vue/test-utils';
import CoFKey from '@/components/KeySigns.vue';

describe('KeySigns Test', () => {
  test('should check if flat key', () => {
    const comp = shallowMount(CoFKey, {
      propsData: {
        akey: {
          nbrOfSigns: 2,
          signType: '#',
        },
      },
    });
    expect(comp.vm.showFlat).toBe(false);
    expect(comp.findAll('.sharps').isVisible()).toBe(true);
    expect(comp.findAll('.flats').isVisible()).toBe(false);
  });
});
