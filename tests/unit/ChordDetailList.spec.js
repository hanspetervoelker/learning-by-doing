import { shallowMount } from '@vue/test-utils';
import cdl from '@/components/ChordDetailList.vue';
import cof from '../../src/lib/circleOfFifths';
import cd from '@/components/ChordDetail.vue';

describe('list test', () => {
  test('', () => {
    const spy = jest.fn();
    const comp = shallowMount(cdl, {
      propsData: {
        chordTypeList: ['MajorSeventh', 'MinorSeventh'],
        akey: cof.keys[0],
        scaleFormula: [0, 2, 4, 5, 7, 9, 11],
      },
    });
    comp.vm.$on('chordlist-click', spy);
    comp.findAll(cd).at(0).vm.$emit('chord-click', 'Cm', 'Cm7', {
      extension: 'maj7',
      formula: [0, 4, 7, 10],
      intervals: ['1', '3', '5', '7'],
      name: '',
    });
    expect(comp.emitted()['chordlist-click'][0][0].notesDetail.name).toBe('C');
  });
});
