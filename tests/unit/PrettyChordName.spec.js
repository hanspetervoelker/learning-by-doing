import { shallowMount } from '@vue/test-utils';
import pcn from '@/components/PrettyChordName.vue';

describe('PrettyChordName Test', () => {
  test('should replace', () => {
    const stub = jest.fn();
    const comp = shallowMount(pcn, {
      propsData: {
        chordName: 'Abm',
        chordExtension: '7b5',
      },
    });
    expect(comp.text()).toContain('♭');
    comp.setMethods({ prettyfy: stub });
    expect(stub).toBeCalledTimes(2);
  });
  test('should call handleClick', () => {
    const stub = jest.fn();
    const comp = shallowMount(pcn, {
      propsData: { chordName: 'Abm', chordExtension: '7b5' },
    });
    comp.setMethods({ handleClick: stub });
    comp.find('p').trigger('click');
    expect(stub).toHaveBeenCalledTimes(1);
  });
  test('should emit', () => {
    const spy = jest.fn();
    const comp = shallowMount(pcn, {
      propsData: { chordName: 'Abm', chordExtension: '7b5' },
      mocks: spy,
    });
    comp.vm.$on('chord-name-click', spy);
    comp.find('p').trigger('click');
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith('Abm', 'Abm7b5');
    // expect(spy).toHaveBeenCalledWith(1, 'Key');
  });
});
