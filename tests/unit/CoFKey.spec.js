import { shallowMount } from '@vue/test-utils';
import CoFKey from '@/components/CoFKey.vue';

describe('CoFKey.vue', () => {
  const keyname = 'Key';
  const keyPosition = 1;
  const wrapper = shallowMount(CoFKey, {
    propsData: { keyname, keyPosition },
  });
  test('renders props correctly', () => {
    expect(wrapper.props().keyname).toBe(keyname);
    expect(wrapper.props().keyPosition).toBe(keyPosition);
    expect(wrapper.contains('span')).toBe(true);
  });

  test('styles and angles are correct computed', () => {
    expect(wrapper.find('li').attributes().style).toContain('transform: rotate(15deg) skewY(-60deg)');
    expect(wrapper.find('strong').attributes().style).toContain('transform: rotate(-30deg)');
    // Hex-Value is changed to RGB when injected over JS
    expect(wrapper.find('strong').attributes().style).toContain('color: rgb(52, 52, 52)');
  });

  test('calls handleClick when clicked', () => {
    const stub = jest.fn();
    wrapper.setMethods({ handleClick: stub });
    wrapper.find('li').trigger('click');
    expect(stub).toBeCalled();
  });

  test('triggers an event when a handleClick method is called', () => {
    const spy = jest.fn();
    const wrapper2 = shallowMount(CoFKey, {
      propsData: { keyname, keyPosition },
      mocks: spy,
    });

    wrapper2.vm.$on('cof-key-click', spy);
    wrapper2.find('li').trigger('click');
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(1, 'Key');
  });

  test('should render correct active state', () => {
    const selected = 1;
    const wrapper2 = shallowMount(CoFKey, {
      propsData: { keyname, keyPosition, selected },
    });
    // Hex-Value is changed to RGB when injected over JS
    expect(wrapper2.find('span').attributes().style).toContain('background-color: rgb(52, 52, 52)');
    expect(wrapper2.find('strong').attributes().style).toContain('color: rgb(241, 241, 241)');
  });
});
