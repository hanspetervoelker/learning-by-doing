import Key from '../../src/lib/keys';
import { Scale } from '../../src/lib/scales';

describe('Scaleclass-Test', () => {
  const s = new Scale();
  test('should set defaults when no parameters', () => {
    expect(s.name).toBe('ionian');
    expect(s.formula[3]).toBe(5);
  });
  test('should give C Scale in default', () => {
    const notes = s.getScaleNotes(new Key());
    expect(notes[0]).toBe('C');
    expect(notes[3]).toBe('F');
    expect(notes[4]).toBe('G');
  });
});
