import { shallowMount } from '@vue/test-utils';
import sil from '@/components/scalesInfoList.vue';
import cof from '../../src/lib/circleOfFifths';
import KeySigns from '@/components/KeySigns.vue';
import { churchModes } from '../../src/lib/scales';

describe('scaleinfo Test', () => {
  const scaleList = [
    {
      name: 'dorian',
      formula: [0, 2, 3, 5, 7, 9, 10],
      scaleintervals: ['1', '2', 'b3', '4', '5', '6', 'b7'],
      offset: 10,
      scalepos: 2,
    },
  ];
  const thekey = cof.keys[7];

  test('should replace and use offset', () => {
    const comp = shallowMount(sil, {
      propsData: {
        akey: thekey,
        scales: scaleList,
        useKeyOffset: true,
      },
    });
    const ks = comp.find(KeySigns);
    expect(comp.find(KeySigns).exists()).toBe(true);
    expect(ks.props().akey.notes[0]).toBe('B');
  });
  test('should not use offset', () => {
    const comp = shallowMount(sil, {
      propsData: {
        akey: thekey,
        scales: scaleList,
        useKeyOffset: false,
      },
    });
    const ks = comp.find(KeySigns);
    expect(comp.find(KeySigns).exists()).toBe(true);
    expect(ks.props().akey.notes[0]).toBe('Db');
  });
  test('should use offset', () => {
    const comp = shallowMount(sil, {
      propsData: {
        akey: thekey,
        scales: churchModes,
        useKeyOffset: true,
      },
    });
    const ks = comp.find(KeySigns);
    expect(comp.find(KeySigns).exists()).toBe(true);
    expect(ks.props().akey.notes[0]).toBe('Db');
  });
});
