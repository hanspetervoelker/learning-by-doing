import cof from '../../src/lib/circleOfFifths';

describe('Circle of Fifths Test', () => {
  test('cof should contain 24 Keys', () => {
    expect(cof.keys.length).toBe(24);
  });
  test('test of getByKeyAndName', () => {
    const key1 = cof.getByKeyAndName('major', 'D');
    expect(key1.name).toBe('D');
    const key2 = cof.getByKeyAndName('major', 'X');
    expect(key2).toBe(undefined);
    const key3 = cof.getByKeyAndName('minor', 'A');
    expect(key3.name).toBe('A');
    expect(key3.notes[3]).toBe('C');
  });
  test('test of getByKeyAndPosition', () => {
    const key = cof.getByKeyAndPosition('major', 2);
    expect(key.name).toBe('D');
  });
});
