import { shallowMount } from '@vue/test-utils';
import piano from '@/components/KeyPiano.vue';
import cof from '../../src/lib/circleOfFifths';

describe('piano Test', () => {
  const formula = [0, 2, 4, 5, 7, 9, 11];
  const akey = cof.keys[7];
  test('', () => {
    const comp = shallowMount(piano, {
      propsData: {
        formula,
        akey,
      },
    });
    expect(comp.vm.showKeys[0]).toBe('');
    expect(comp.vm.showKeys[1]).toBe('Db');
  });
});
