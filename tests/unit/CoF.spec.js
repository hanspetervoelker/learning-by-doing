import { shallowMount } from '@vue/test-utils';
import cof from '@/components/CoF.vue';
import ring from '@/components/CoFRing.vue';

describe('circle test', () => {
  test('', () => {
    const comp = shallowMount(cof, {
      propsData: {
        majorKeys: [],
        minorKeys: [],
      },
    });
    comp.findAll(ring).at(0).vm.$emit('cof-ring-click');
  });
});
