import { shallowMount } from '@vue/test-utils';
import cd from '@/components/ChordDetail.vue';
import pcn from '@/components/PrettyChordName.vue';
import cof from '../../src/lib/circleOfFifths';

describe('ChordDetail test', () => {
  test('', () => {
    const stub = jest.fn();
    const comp = shallowMount(cd, {
      propsData: {
        akey: cof.keys[0],
        chordType: 'MajorSeventh',
      },
    });
    comp.vm.$on('chord-click', stub);
    comp.find(pcn).vm.$emit('chord-name-click', 'Abm', 'Abm7b5');
    expect(stub).toHaveBeenCalled();
    expect(stub).toHaveBeenCalledWith('Abm', 'Abm7b5', {
      extension: 'maj7',
      formula: [0, 4, 7, 11],
      intervals: ['1', '3', '5', '7'],
      name: '',
    });
  });
});
