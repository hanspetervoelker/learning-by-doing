import chords, { majorScaleChords } from '../../src/lib/chords';

describe('Chords-Test', () => {
  test('default', () => {
    expect(chords.HalfDiminishedSeventh).toBeDefined();
    expect(Object.keys(chords).length).toBe(8);
    expect(chords[majorScaleChords[2]]).toBeDefined();
  });
});
