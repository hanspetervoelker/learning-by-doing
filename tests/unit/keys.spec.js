import Key from '../../src/lib/keys';

describe('Key-Test', () => {
  const defKey = new Key();
  test('Class without parameters defaults to C', () => {
    expect(defKey.name).toBe('C');
    expect(defKey.signType).toBe('♮');
    expect(defKey.offset).toBe(0);
  });
  test('Default class has sharp notes', () => {
    expect(defKey.notes[1]).toBe('C#');
    expect(defKey.key).toBe('major');
    expect(defKey.notes.filter(note => note.indexOf('#') > 0).length).toBe(5);
  });
  test('wrong noteType defaults to sharp', () => {
    const k = new Key('bla', 0, '♯', 'notExistent', 'name', 1);
    expect(k.notes.length).toBe(12);
    expect(k.notes[1]).toBe('C#');
  });
  test('key returns correct notes array', () => {
    const k = new Key('major', 2, '♯', 'sharps', 'D', 3);
    expect(k.notes[0]).toBe('D');
    expect(k.notes[10]).toBe('C');
    expect(k.offset).toBe(2);
  });
  test('getScaleByFormula should give correct major scale', () => {
    const k = new Key('major', 2, '♯', 'sharps', 'D', 3);
    const scale = k.getScaleByFormula([0, 2, 4, 5, 7, 9, 11]);
    expect(scale[0]).toBe('D');
    expect(scale[2]).toBe('F#');
  });
});
