# HPV' Music-Vue-Components

[![pipeline status](https://gitlab.com/hanspetervoelker/learning-by-doing/badges/master/pipeline.svg)](https://gitlab.com/hanspetervoelker/learning-by-doing/commits/master)
[![coverage report](https://gitlab.com/hanspetervoelker/learning-by-doing/badges/master/coverage.svg)](https://gitlab.com/hanspetervoelker/learning-by-doing/commits/master)

Music Components created while learning Vue. This components are not production ready, but can be used for your own experiments with Vue. I tried to document as much as I can to give beginners in Vue like me a set of code to begin.

## Documentation

### Components

Link: [Music-Vue-Pages](https://hanspetervoelker.gitlab.io/learning-by-doing/index.html)

Interactive documentation of the components (created with vue-styleguide)

### Modules

Link: [Music-Vue-Wiki](https://gitlab.com/hanspetervoelker/learning-by-doing/wikis/home)

JSDoc Documentation of the modules created with documentation.js (files manually separated)

## Docker container
If you want to serve the components documentation in container (nginx with size of 7.84 MiB) then you can pull the image from the registry of this project
```bash
docker pull registry.gitlab.com/hanspetervoelker/learning-by-doing:latest
```
or build the container using the Dockerfile. The build and run command can be found in [this snippet](https://gitlab.com/hanspetervoelker/learning-by-doing/snippets/1792822) in the snippets section of this project.

> *Thanks to Sara Vieira and her blog about serving static content "Deploy your Create React App with Docker and Nginx" for providing the nginx.conf*

![music-vue](/uploads/332e38effd57d554ae7b2a96a690be2d/music-vue.png)

## Project setup

```npm
npm install
```

### Compiles and hot-reloads for development

```npm
npm run serve
```

### Compiles and minifies for production

```npm
npm run build
```

### Lints and fixes files

```npm
npm run lint
```

### Run your unit tests

```npm
npm run test:unit
```

### Build component documentation

```npm
npm run styleguide:build
```

## To Do

- [ ] include linting in CI/CD
- [ ] include SAST stage in CI/CD
- [ ] include Dependency Scanning in CI/CD
- [ ] include Code Climate check in CI/CD
- [ ] create some components for guitar (fretboard, chord-chart)
- [ ] push the docker image with CI/CD